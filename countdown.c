///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   ./countdown
//       Reference time: Tue Jan 21 04:26:07 PM HST 2014
//       Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 57 
//       Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 58 
//       Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 59 
//       Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 0 
//       Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 1 
//       Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 2 
//       Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 3 
//
// @author Liam Tapper<tliam@hawaii.edu>
// @date   22/02/2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <time.h>
#include <unistd.h>

struct timeStorage{
int years, days, hours, mins, secs;
};

struct timeDif{
int yearDif, dayDif, hourDif, minDif, secDif;
};


struct timeStorage timeStore;
struct tm times;
struct timeDif timeDiff;
struct tm * localTime;
time_t prevTime;

void waitTime(){
   sleep(1);
   time(&prevTime); //update time through every iteration such that it stays updated
   localTime = localtime(&prevTime); //reassign as needed
                                     
                                     
                                     
   if (timeStore.years - localTime->tm_year >= 0)
      timeDiff.yearDif = timeStore.years - localTime->tm_year;
   else 
      timeDiff.yearDif = -timeStore.years + localTime->tm_year;

   if (timeStore.days - localTime->tm_mday >= 0)
      timeDiff.dayDif = timeStore.days - localTime->tm_mday;
   else 
      timeDiff.dayDif = -timeStore.days + localTime->tm_mday;

   if (timeStore.hours - localTime->tm_hour >= 0)
      timeDiff.hourDif = timeStore.hours - localTime->tm_hour;
   else
      timeDiff.hourDif = -timeStore.hours + localTime->tm_hour;

   if (timeStore.mins - localTime->tm_min >= 0)
      timeDiff.minDif = timeStore.mins - localTime->tm_min;
   else
      timeDiff.minDif = -timeStore.mins + localTime->tm_min;
   
   if (timeStore.secs - localTime->tm_sec >= 0)
      timeDiff.secDif = timeStore.secs - localTime->tm_sec;
   else
      timeDiff.secDif = -timeStore.secs + localTime->tm_sec;
 
   printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n", timeDiff.yearDif, timeDiff.dayDif, 
         timeDiff.hourDif, timeDiff.minDif, timeDiff.secDif);
}

void storeData(){
   timeStore.years = times.tm_year;
   //printf("%d\n", timeStore.years);
   timeStore.days = times.tm_mday;
   timeStore.hours = times.tm_hour;
   timeStore.mins = times.tm_min;
   timeStore.secs =  times.tm_sec;
}




int main() {
   times.tm_year = 2014-1900; 
   times.tm_mon = 0;
   times.tm_mday = 21;
   times.tm_hour = 4;
   times.tm_min = 26;
   times.tm_sec = 7;
   prevTime = mktime(&times);
   printf("Reference Time: %s\n", ctime(&prevTime));
   storeData(); // stores needed tm data constants so I can be overwritten
   time(&prevTime); //found through research and a replacement for time(0) as I dont know how to assign that to a struct
   localTime = localtime(&prevTime); // assigning it to a pointer (localtime converts time_t to struct pointer)
 //  printf("%d\n", localTime->tm_year);
//   printf ("Current local time and date: %s\n", asctime(localTime));
   //asctime converts contents of struct into a string (calenderi-esc format IE. day, month, number day   time year)
   //I needed to add another pointer because to use local time it needs to be an array 
   //local time overrides times and thus a storage string was created
   //I learned at the end of this lab that the pointer is "seperate" in a way. The pointer didnt overwrite the 
   //struct but before when my pointer was above the mkTime it was overwritten 
   //I need to learn why that happens 
   //I had to do extensive digging to try and use two tm structures or rather two different times. 
   //I am still lost when I comes to overwritting the data, but I have it working and it is due very soon
   //I'll be doing research to better understand this issue

   //end by simulating EOF (END OF FILE) via ctrl + C thanks EE160!
   int i;
   while (i != EOF)
   waitTime();

   return 0;
}


